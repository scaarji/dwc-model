'use strict';
var _ = require('lodash'),
    inspector = require('schema-inspector'),
    uuid = require('uuid');

require('./validators');

/**
 * Returns new model with given parameters
 * @param {String} name         Model name
 * @param {Object} schema       The schema of new model
 * @param {Object} [defaults]   Object that contains default values
 *                              (possibly as functions) for object fields
 * @param {Object} [options]    Additional options for model
 * @param {Boolean} [options.id] Indicates if id should be created as uuid
 */
var createModel = function createModel(name, schema, defaults, options) {
    options = options || {};
    defaults = defaults || {};

    /**
     * Constructor function for new model
     * @param {Object} data Data to populate new object with
     * @return {Model} New instance of model
     */
    var Model = function(data) {
        data = data || {};
        if (options.id !== false) {
            data.id = data.id || uuid.v4();
        }

        inspector.sanitize(schema, data);
        this.fillDefaults();

        _.extend(this, data);
    };

    // define name of current model
    Model.modelName = name;

    /**
     * Converts this instance to plain object
     * @return {Object}
     */
    Model.prototype.toObject = function() {
        var data = _.clone(this);

        inspector.sanitize(schema, data);

        return data;
    };

    /**
     * Validates data currently in object
     * @return {Object} Result object, contains boolean valid and string message
     */
    Model.prototype.isValid = function() {
        var result = inspector.validate(schema, this.toObject());

        if (result.valid) {
            return {
                valid: true,
                message: null
            };
        }

        return {
            valid: false,
            message: result.format()
        };
    };

    /**
     * Populates missing fields with default values
     * @return {Model}
     */
    Model.prototype.fillDefaults = function() {
        var _this = this;
        _.each(defaults, function(v, k) {
            if (_.isNull(_this[k]) || _.isUndefined(_this[k])) {
                _this[k] = _.isFunction(defaults[k]) ? defaults[k].call(_this) : defaults[k];
            }
        });

        return _this;
    };

    return Model;
};

module.exports = createModel;
