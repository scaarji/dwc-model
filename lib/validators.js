'use strict';
var _ = require('lodash'),
    inspector = require('schema-inspector');

inspector.Validation.extend({
    enum: function(schema, candidate) {
        if (!_.contains(schema.$enum, candidate)) {
            this.report('must be one of [' + schema.$enum.join(', ') + ']');
        }
    }
});

